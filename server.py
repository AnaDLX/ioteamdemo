from flask import Flask, Response, request, jsonify
from flask_restful import Resource, Api
from flask_restful.utils import cors
from pymongo import MongoClient
from bson.json_util import dumps
from flask_cors import CORS
import json
from bson import ObjectId

client = MongoClient('mongodb://localhost:27017/')
db = client['ioteam']
history = db['history']

app = Flask(__name__)
api = Api(app)
api.decorators=[cors.crossdomain(origin='*')]
CORS(app)

class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)

class History(Resource):
    def get(self):
        result_json = []
        for obj in history.find():
            result_json.append(obj)
        json = JSONEncoder().encode(result_json)
        resp = Response(json, status=200, mimetype='application/json')
        return resp

    def post(self):
        history.insert_one(json.loads(request.data))
        return 200

api.add_resource(History, '/')

if __name__ == '__main__':
    app.run(port='5002')
